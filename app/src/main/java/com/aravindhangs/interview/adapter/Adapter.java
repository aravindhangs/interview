package com.aravindhangs.interview.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.aravindhangs.interview.R;
import com.aravindhangs.interview.model.CountriesModel;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private final LayoutInflater inflater;
    private ArrayList<CountriesModel> dataModelArrayList;
    private final Activity activity;

    public Adapter(Context ctx, Activity act, ArrayList<CountriesModel> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.dataModelArrayList = dataModelArrayList;
        this.activity = act;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_layout, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.capital.setText("Capital : "+dataModelArrayList.get(position).getCapital());

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, capital;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_name);
            capital = itemView.findViewById(R.id.tv_capital);

        }

    }

    public void updateList(ArrayList<CountriesModel> updateList){
        dataModelArrayList = updateList;
        notifyDataSetChanged();
    }
}
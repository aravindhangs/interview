package com.aravindhangs.interview.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.aravindhangs.interview.R;
import com.aravindhangs.interview.model.LoginUser;
import com.aravindhangs.interview.viewModel.CommonViewModel;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    EditText txtUserName;
    EditText txtPassword;
    Button btnLogin;
    CommonViewModel commonViewModel;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");

        txtUserName = findViewById(R.id.txtUserName);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);

        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoginUser loginUser = new LoginUser(txtUserName.getText().toString(), txtPassword.getText().toString());

                if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getStrEmailAddress())) {
                    txtUserName.setError("Enter a Valid Username");
                    txtUserName.requestFocus();
                } else if (!loginUser.isEmailValid()) {
                    txtUserName.setError("Enter a Valid Username");
                    txtUserName.requestFocus();
                } else if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getStrPassword())) {
                    txtPassword.setError("Enter a Valid Password");
                    txtPassword.requestFocus();
                } else if (!loginUser.isPasswordLengthGreaterThan5()) {
                    txtPassword.setError("Enter a Valid Password");
                    txtPassword.requestFocus();
                } else {
                    progressDialog.show();
                    commonViewModel.userLogin(loginUser).observe(LoginActivity.this, new Observer<String>() {
                        @Override
                        public void onChanged(String countries) {
                            if (progressDialog.isShowing())
                                progressDialog.dismiss();

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }
        });

    }
}
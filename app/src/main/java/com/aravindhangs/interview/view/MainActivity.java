package com.aravindhangs.interview.view;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aravindhangs.interview.R;
import com.aravindhangs.interview.adapter.Adapter;
import com.aravindhangs.interview.model.CountriesModel;
import com.aravindhangs.interview.viewModel.CommonViewModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Adapter adapter;
    RecyclerView recyclerView;
    ArrayList<CountriesModel> modelRecyclerArrayList;
    CommonViewModel commonViewModel;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        modelRecyclerArrayList = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler_view);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);

        initRecycler();

        commonViewModel.loadData().observe(this, new Observer<ArrayList<CountriesModel>>() {
            @Override
            public void onChanged(ArrayList<CountriesModel> countries) {
                if (countries != null) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    modelRecyclerArrayList = countries;
                    adapter.updateList(modelRecyclerArrayList);
                }
            }
        });
    }

    private void initRecycler() {
        adapter = new Adapter(this, this, modelRecyclerArrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
    }

}
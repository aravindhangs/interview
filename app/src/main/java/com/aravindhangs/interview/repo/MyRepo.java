package com.aravindhangs.interview.repo;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;


import com.aravindhangs.interview.api.RetrofitClient;
import com.aravindhangs.interview.model.CountriesModel;
import com.aravindhangs.interview.model.LoginUser;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MyRepo {

    private final MutableLiveData<ArrayList<CountriesModel>> allCountries;
    private final ArrayList<CountriesModel> countryList;
    private final MutableLiveData<String> LoginAPIResponse;


    public MyRepo(Application application) {
        countryList = new ArrayList<>();
        allCountries = new MutableLiveData<>();
        LoginAPIResponse = new MutableLiveData<>();
    }

    public MutableLiveData<ArrayList<CountriesModel>> callAPI() {

        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://restcountries.eu/rest/v2/region/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).build();

        Call<ResponseBody> call = RetrofitClient.getInstance().getapi(retrofit).countries();
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {

                    try {
                        assert response.body() != null;

                        JSONArray dataArray = new JSONArray(response.body().string());

                        for (int i = 0; i < dataArray.length(); i++) {

                            CountriesModel modelRecycler = new CountriesModel();
                            JSONObject dataobj = dataArray.getJSONObject(i);

                            modelRecycler.setName(dataobj.getString("name"));
                            modelRecycler.setCapital(dataobj.getString("capital"));
                            countryList.add(modelRecycler);
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    allCountries.setValue(countryList);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                allCountries.postValue(null);
            }
        });
        return allCountries;

    }

    public MutableLiveData<String> userLoginAPI(LoginUser loginUser) {

        Gson gson = new Gson();

        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://reqres.in/api/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).build();

        Call<ResponseBody> call = RetrofitClient.getInstance().getapi(retrofit).userLogin(gson.toJson(loginUser));
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 201) {
                    try {
                        assert response.body() != null;
                        JSONObject dataArray = new JSONObject(response.body().string());
                        LoginAPIResponse.setValue(dataArray.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
        return LoginAPIResponse;
    }
}

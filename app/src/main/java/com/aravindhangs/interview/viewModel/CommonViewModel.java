package com.aravindhangs.interview.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


import com.aravindhangs.interview.model.CountriesModel;
import com.aravindhangs.interview.model.LoginUser;
import com.aravindhangs.interview.repo.MyRepo;

import java.util.ArrayList;

public class CommonViewModel extends AndroidViewModel {

    private final MyRepo repository;

    public CommonViewModel(@NonNull Application application) {
        super(application);
        repository = new MyRepo(application);
    }

    public MutableLiveData<ArrayList<CountriesModel>> loadData() {
        return repository.callAPI();
    }

    public MutableLiveData<String> userLogin(LoginUser loginUser) {
        return repository.userLoginAPI(loginUser);
    }
}

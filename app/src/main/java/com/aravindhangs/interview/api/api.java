package com.aravindhangs.interview.api;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface api {

    @GET("asia")
    Call<ResponseBody> countries();

    @POST("users")
    Call<ResponseBody> userLogin(@Body String loginUser);
}

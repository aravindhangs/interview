package com.aravindhangs.interview.api;


import retrofit2.Retrofit;

public class RetrofitClient {

    private static RetrofitClient instance;

    private RetrofitClient() {
    }

    public static synchronized RetrofitClient getInstance() {
        if (instance == null) {
            instance = new RetrofitClient();
        }
        return instance;

    }

    public api getapi(Retrofit retrofit) {
        return retrofit.create(api.class);
    }
}
